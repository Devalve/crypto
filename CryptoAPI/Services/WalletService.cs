﻿using Crypto.Models;
using Nethereum.Web3;
using Microsoft.EntityFrameworkCore;

namespace CryptoAPI.Services
{
    public class WalletService
    {
        public static async Task<List<Wallet>> GetBalancesFromNodeAsync()
        {
            var wallets = new List<Wallet>();
            var httpClient = new HttpClient();

            var web3 = new Web3("https://sepolia.infura.io/v3/64e83f3b305041b0a1d83464110ce665");

            var walletsFromDatabase = await GetWalletsFromDBAsync();

            var tasks = walletsFromDatabase.Select(async wallet =>
            {
                var balance = await web3.Eth.GetBalance.SendRequestAsync(wallet.Address);
                wallet.Balance = Web3.Convert.FromWei(balance);
                return wallet;
            })
            .ToList();

            wallets = (await Task.WhenAll(tasks)).ToList();

            return wallets;
        }

        public static async Task<List<Wallet>> GetWalletsFromDBAsync()
        {
            using var _dbContext = new CryptoDbContext();

            var wallets = await _dbContext.Wallets.ToListAsync();

            return wallets;

        }
    }
}
