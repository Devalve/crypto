﻿using Crypto.Models;

namespace CryptoAPI.Services
{
    public interface IWalletService
    {
        public Task<List<Wallet>> GetBalancesFromNodeAsync();
        public Task<List<Wallet>> GetWalletsFromDBAsync();
    }
}
