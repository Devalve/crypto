﻿using Crypto.Models;
using Microsoft.EntityFrameworkCore;

namespace CryptoAPI
{
    public class CryptoDbContext : DbContext
    {
        public DbSet<Wallet> Wallets { get; set; }
        public CryptoDbContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //TODO заменить на переменные среды
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=postgres;Username=postgres;Password=root");
        }
    }
}
