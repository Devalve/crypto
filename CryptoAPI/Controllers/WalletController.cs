﻿using Crypto.Models;
using CryptoAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace CryptoAPI.Controllers
{


    [ApiController]
    [Route("api/wallets")]
    public class WalletsController : Controller
    {
        //private readonly WalletService walletService;

        //public WalletsController(WalletService walletService)
        //{
        //    this.walletService = walletService;
        //}

        [HttpPost]
        [Route("/GetWalletsFromDB")]
        public async Task<ActionResult<List<Wallet>>> GetWallets()
        {
            var wallets = await WalletService.GetWalletsFromDBAsync();
            return Ok(wallets);
        }

        [HttpPost]
        [Route("/GetBalancesFromNode")]
        public async Task<ActionResult<List<Wallet>>> GetBalancesAsync()
        {
            return await WalletService.GetBalancesFromNodeAsync();
        }
    }


}
